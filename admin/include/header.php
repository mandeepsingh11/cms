<nav class="navbar navbar-inverse navbar-fixed-top">
			  <div class="container-fluid">
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="#">Brand</a>
			    </div>

			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
			      
			      
			      <ul class="nav navbar-nav navbar-right">
			        <li><a href="users.php"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Post</a></li>
			        <li><a href="add-user.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Add User</a></li>
			        <li><a href="profile.php"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></li>
			        <li><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
			      </ul>
			    </div>
			  </div>
			</nav>