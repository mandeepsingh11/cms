<?php
session_start();
if(!isset($_SESSION['username'])) {
			
		
		header("Location:login.php");
				
	}
?>

	<!DOCTYPE html>
<html>
<head>
	
	<?php
	

	 include 'include/head-data.php';
	
	
	
	?>
</head>
<body>
	<div id="wrapper">
			<?php include 'include/header.php' ?>		
			<div class="container body-section">
				<div class="row">
						<?php include 'include/sidebar.php' ?>
					<div class="col-md-9">
						<h1><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard<small>Stastics Overview</small></h1><hr>
						<ol class="breadcrumb">
							  <li class="active"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</li>
							  
						</ol>
						<div class="row tag-boxes">
							<div class="col-md-3">
								<div class="panel panel-primary">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
											<i class="fa fa-comment fa-3x" aria-hidden="true"></i>
										</div>
										<div class="col-xs-9">
											<div class="text-right huge">
												11
											</div>
											<div class="text-right">
												New comments
											</div>
										</div>
										</div>
									</div>
									<a href="">
										<div class="panel-footer">
											<span class="pull-left">
												View All comments
											</span>
											<span class="pull-right">
												<i class="fa fa-arrow-circle-right"></i>
											</span>
											<div class="clearfix">
												
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-3">
								<div class="panel panel-red">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
											<i class="fa fa-file-text fa-3x" aria-hidden="true"></i>
										</div>
										<div class="col-xs-9">
											<div class="text-right huge">
												20
											</div>
											<div class="text-right">
												All Posts
											</div>
										</div>
										</div>
									</div>
									<a href="">
										<div class="panel-footer">
											<span class="pull-left">
												View All Posts
											</span>
											<span class="pull-right">
												<i class="fa fa-arrow-circle-right"></i>
											</span>
											<div class="clearfix">
												
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-3">
								<div class="panel panel-green">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
											<i class="fa fa-folder-o fa-3x" aria-hidden="true"></i>
										</div>
										<div class="col-xs-9">
											<div class="text-right huge">
												4
											</div>
											<div class="text-right">
												categories
											</div>
										</div>
										</div>
									</div>
									<a href="">
										<div class="panel-footer">
											<span class="pull-left">
												View All categories
											</span>
											<span class="pull-right">
												<i class="fa fa-arrow-circle-right"></i>
											</span>
											<div class="clearfix">
												
											</div>
										</div>
									</a>
								</div>
							</div>
							<div class="col-md-3">
								<div class="panel panel-orange">
									<div class="panel-heading">
										<div class="row">
											<div class="col-xs-3">
											<i class="fa fa-user-plus fa-3x" aria-hidden="true"></i>
										</div>
										<div class="col-xs-9">
											<div class="text-right huge">
												2
											</div>
											<div class="text-right">
												All Users
											</div>
										</div>
										</div>
									</div>
									<a href="">
										<div class="panel-footer">
											<span class="pull-left">
												View All Users
											</span>
											<span class="pull-right">
												<i class="fa fa-arrow-circle-right"></i>
											</span>
											<div class="clearfix">
												
											</div>
										</div>
									</a>
								</div>
							</div><hr>
							<h3 class="Table-heading">
								New Users
							</h3>
								<table class="table table-hover table-striped">
									<thead>
										<tr>
										<th>SR #</th>
										<th>Date </th>
										<th>Name</th>
										<th>UserName</th>
										<th>Role</th>
									</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>11 june 1996</td>
											<td>Mandeep Singh</td>
											<td>Mandeep1196</td>
											<td>Admin</td>
										</tr>
										<tr>
											<td>1</td>
											<td>11 june 1996</td>
											<td>Mandeep Singh</td>
											<td>Mandeep1196</td>
											<td>Admin</td>
										</tr>
										<tr>
											<td>1</td>
											<td>11 june 1996</td>
											<td>Mandeep Singh</td>
											<td>Mandeep1196</td>
											<td>Admin</td>
										</tr>
										<tr>
											<td>1</td>
											<td>11 june 1996</td>
											<td>Mandeep Singh</td>
											<td>Mandeep1196</td>
											<td>Admin</td>
										</tr>
										<tr>
											<td>1</td>
											<td>11 june 1996</td>
											<td>Mandeep Singh</td>
											<td>Mandeep1196</td>
											<td>Admin</td>
										</tr>
									</tbody>
								</table>
								<a href="#" class="btn btn-primary">View All users</a>
								<hr>
								<h3 class="Table-heading">
								New Posts
							</h3>
								<table class="table table-hover table-striped">
									<thead>
										<tr>
										<th>SR #</th>
										<th>Date </th>
										<th>Post title</th>
										<th>Category</th>
										<th>Views</th>
									</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>11 june 1996</td>
											<td>Creating Blog using php and bootsrap</td>
											<td>Tutorial</td>
											<td>113</td>
										</tr>
										<tr>
											<td>1</td>
											<td>11 june 1996</td>
											<td>Creating Blog using php and bootsrap</td>
											<td>Tutorial</td>
											<td>113</td>
										</tr>
										<tr>
											<td>1</td>
											<td>11 june 1996</td>
											<td>Creating Blog using php and bootsrap</td>
											<td>Tutorial</td>
											<td>113</td>
										</tr>
										<tr>
											<td>1</td>
											<td>11 june 1996</td>
											<td>Creating Blog using php and bootsrap</td>
											<td>Tutorial</td>
											<td>113</td>
										</tr>
										<tr>
											<td>1</td>
											<td>11 june 1996</td>
											<td>Creating Blog using php and bootsrap</td>
											<td>Tutorial</td>
											<td>113</td>
										</tr>
									</tbody>
								</table>
								<a href="#" class="btn btn-primary">View All Posts</a>
								<hr>

						</div>
						
					</div>
				</div>
				
			</div>
			
	</div>
</body>
</html>