<!DOCTYPE html>
<html>
<head>
	<?php include 'include/head-data.php' ?>
<body>
	<div id="wrapper">
					<?php include 'include/header.php' ?>
			<div class="container body-section">
				<div class="row">
						<?php include 'include/sidebar.php' ?>
					<div class="col-md-9">
						<h1><i class="fa fa-folder-o" aria-hidden="true"></i>Categories<small>Categories Overview</small></h1><hr>
						<ol class="breadcrumb">
							  <li class="active"><a href="index.html"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>
							 <li class="active"><i class="fa fa-folder-o" aria-hidden="true"></i> Catgeories</li>						  
						</ol>
						<div class="row">
							<div class="col-md-6">
								<form action="">
									<div class="form-group">
										<label for="categories">Category Name
										</label>
										<input type="text" name="" class="form-control" placeholder="category name">
									</div>
									<input type="submit" name="" value="Add Category" class="btn btn-primary">
								</form>
							</div>
							<div class="col-md-6">
								<table class="table table-hover table-striped">
									<thead>
										<tr>
											<th>Serial #</th>
											<th>Category</th>
											<th>Posts</th>
											<th>Edit</th>
											<th>Delete</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Articles</td>
											<td>20</td>
											<td><a href="#"><i class="fa fa-pencil"></i></a></td>
											<td><a href="#"><i class="fa fa-times"></i></a></td>
										</tr>
											<td>1</td>
											<td>Articles</td>
											<td>20</td>
											<td><a href="#"><i class="fa fa-pencil"></i></a></td>
											<td><a href="#"><i class="fa fa-times"></i></a></td>
										</tr>
											<td>1</td>
											<td>Articles</td>
											<td>20</td>
											<td><a href="#"><i class="fa fa-pencil"></i></a></td>
											<td><a href="#"><i class="fa fa-times"></i></a></td>
										</tr>
											<td>1</td>
											<td>Articles</td>
											<td>20</td>
											<td><a href="#"><i class="fa fa-pencil"></i></a></td>
											<td><a href="#"><i class="fa fa-times"></i></a></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						</div>
						
					</div>
				</div>
				
			</div>
			<?php include 'include/footer.php' ?>
	</div>
</body>
</html>