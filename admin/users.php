<!DOCTYPE html>
<html>
<head>
	<?php include 'include/head-data.php' ?>
</head>
<body>
	<div id="wrapper">
		<?php include 'include/header.php' ?>			
			
			<div class="container-fluid body-section">
				<div class="row">
						<?php include 'include/sidebar.php' ?>	
					<div class="col-md-9">
						<h1><i class="fa fa-user" aria-hidden="true"></i>Users<small>Users Overview</small></h1><hr>
						<ol class="breadcrumb">
							  <li><a href="index.html"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>
							 <li class="active"><i class="fa fa-user" aria-hidden="true"> </i>Users</li>
							  
						</ol>
						<?php
						$query="SELECT * FROM users ORDER BY id DESC";
						$run=mysqli_query($connection,$query);
						if (mysqli_num_rows($run)>0) {
							

						?>
						<div class="row">
							<div class="col-sm-8">
								<form class="action">
									<div class="row">
										<div class="col-xs-4">
											<div class="form-group">
												<select name="" id="" class="form-control">
													<option value="delete">Delete</option>
													<option value="Author">
														Change to Author
													</option>
													<option value="Admin">
														change to Admin
													</option>
												</select>
											</div>
										</div>
										<div class="col-xs-8">
											<input type="submit" name="" class="btn btn-success" value="Apply">
											<a href="" class="btn btn-primary">Add New user</a>
										</div>
										
									</div>
								</form>
							</div>
							
						</div>
						<table class="table table-hover table-striped table-borderd">
							<thead>
								<tr>
									<th>Sr #</th>
									<th>Date</th>
									<th>name</th>
									<th>Username</th>
									<th>Email</th>
									<th>Image</th>
									<th>password</th>
									<th>Role</th>
									<th>Posts</th>
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>

							<tbody>
							<?php
								while($row=mysqli_fetch_array($run))
								{
									$id=$row['id'];
										$first_name=$row['first_name'];
										$last_name=$row['last_name'];
										$date=getdate($row['date']);
										$day = $date['mday'];
										$month= $date['month'];
										$year = $date['year'];
										$email=$row['email'];
										
										$image=$row['image'];
										$password=$row['password'];
										$details=$row['details'];
										$role=$row['role'];
										$username=$row['username'];
								
							?>
								<tr>
									<td><?php echo $id?></td>
									<td><?php echo "$day $month $year"?></td>
									<td><?php echo "$first_name $last_name"?></td>
									<td><?php echo $username?></td>
									<td><?php echo $email?></td>
									<td><img src="img/<?php echo $image?>"n width="60px"></td>
									<td><?php echo $password?></td>
									<td><?php echo $role?></td>
									<td>11</td>
									<td><a href="add-user.php?edit=<?php echo $id?>"><i class="fa fa-pencil"></i></a></td>
									<td><a href="users.php?edit=<?php echo $id?>"><i class="fa fa-times"></i></a></td>
								</tr>
								<?php
								}?>
							</tbody>
							
						</table>
						<?php
					}
							else
						{
							echo "<center><h2>No users is available </h2></center>";
						}
						?>

						</div>

						
					</div>
				</div>
				
			</div>
			<?php include 'include/footer.php' ?>	
	</div>
</body>
</html>