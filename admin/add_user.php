<!DOCTYPE html>
<html>
<head>
	<?php 
	include "include/head-data.php";
	?>
</head>
<body>
	<div id="wrapper">
		<?php include 'include/header.php' ?>			
			
			<div class="container-fluid body-section">
				<div class="row">
						<?php include 'include/sidebar.php' ?>	
					<div class="col-md-9">
						<h1><i class="fa fa-user-plus" aria-hidden="true"></i>Add New Users<small>add new users</small></h1><hr>
						<ol class="breadcrumb">
							  <li><a href="index.html"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a></li>
							 <li class="active"><i class="fa fa-user" aria-hidden="true"> </i>Add New User</li>
							  
						</ol>
						<?php

							if(isset($_POST['submit']))
							{

								echo $date=time();
								echo$first_name=mysqli_real_escape_string($connection,$_POST['first-name']);
								echo$last_name=mysqli_real_escape_string($connection,$_POST['last-name']);
								echo$username=mysqli_real_escape_string($connection,$_POST['username']);
								
								echo$email=mysqli_real_escape_string($connection,$_POST['email']);
								echo$password=mysqli_real_escape_string($connection,$_POST['password']);
								echo $image=$_POST['image'];

								
								echo$role=$_POST['role'];
								
								$check_query="SELECT * FROM users where username='$username' or email='$email'";
								$check_run=mysqli_query($connection,$check_query);
								


								if(empty($first_name) or empty($last_name) or empty($username) or empty($email) or empty($password))
								{
									$error="All fields are required";
								}
								

								else if(mysqli_num_rows($check_run)>0)
								{
									echo "username or email already exists";
								}
								
									else
										{
												$insert_query="INSERT INTO `users` (`id`, `date`,`image`, `username`, `first_name`, `last_name`, `email`, `password`, `role`, `details`, `salt`) VALUES (NULL, '$date','$image' '$username', '$first_name', '$last_name', '$email',  '$password', '$role')";	
												if(mysqli_query($connection,$insert_query))
												{
													$msg="user has been added";
												}								
												else
												{
													$error="user has not been added";
												}	
										}
								
							}
						 ?>
						<div class="col-md-8">
								<form action="" method="post">
							<div class="form-group">
								<label for="first-name">
									First Name
								</label>
								<?php
									if (isset($error)) {
										echo $error;
									}
									elseif (isset($msg)) {
										echo $msg;
									}
									
								?>
								<input type="text" id="first-name" name="first-name" class="form-control" placeholder="First Name">
							</div>
							<div class="form-group">
								<label for="last-name">
									last Name
								</label>
								<input type="text" name="last-name" id="last-name"  class="form-control" placeholder="Last Name">
							</div>
							<div class="form-group">
								<label for="first-name">
									User Name
								</label>
								<input type="text" name="username" id="user-name" class="form-control" placeholder="user Name">
							</div>
							<div class="form-group">
								<label for="email">
									Email
								</label>
								<input type="text" name="email" id="email" class="form-control" placeholder="Eamil" value="">
							</div>
							<div class="form-group">
								<label for="password">
									Password
								</label>
								<input type="password" id="password" name="password" class="form-control" placeholder="password" value="">
							</div>
							
							<div class="form-group">
								<label for="role">
									Role
								</label>
								<select name="role" id="role" class="form-control">
								<option value="author">Author</option>
								<option value="admin">Admin</option>
							</select>	
							</div>
							
							<div class="form-group">
								<label for="password">
									image
								</label>
								<input type="file" id="password" name="image" class="form-control"  value="">
							</div>
							<!-- <div class="form-group">
								<label for="image">
									profile picture
								</label>
								<input type="file" name="image" class="form-control" placeholder="password">
							</div> -->
							<input type="submit" name="submit" value="Add new user" class="btn btn-success">
						</form>
						</div>
						<div class="col-md-4"></div>

						</div>

						
					</div>
				</div>
				
			</div>
			<?php include 'include/footer.php' ?>	
	</div>
</body>
</html>