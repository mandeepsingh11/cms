<!DOCTYPE html>
<html>
<head>
	<?php include 'include/head-data.php' ?>
<body>
	<?php include "include/header.php" ?>
	<?php
	if (isset($_GET['post_id'])) {
	 	$post_id=$_GET['post_id'];
	 	$views_query="UPDATE posts SET views = views+1 WHERE  id = $post_id";
	 	mysqli_query($connection,$views_query);
	 	$query="SELECT * FROM posts WHERE status ='publish' and id='$post_id'";
	 	$run=mysqli_query($connection,$query);
	 	if (mysqli_num_rows($run)>0) {

	 		$row=mysqli_fetch_array($run);
	 		$id = $row['id'];
								$Date = getdate($row['Date']);
								$day = $Date['mday'];
								$month= $Date['month'];
								$year = $Date['year'];
								$title = $row['title'];
								$author = $row['author'];
								$author_image = $row['author_image'];
								$image = $row['image'];
								$categories = $row['categories'];
								$tags = $row['tags'];
								$post_data = $row['post_data'];
								
	 	}
	 	else
	 	{
	 		header('Location:index.php');
	 	}
	 } ?>
	<div class="jumbotron jumbotron-div details">
		<div class="container">
			<h1 class="animated fadeInLeft">
				<span class="jumbotron-heading">Post Page
			</h1>
			<p class="animated fadeInRight">
				Page for reading Latest posts
			</p>

		</div>
	</div>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-xs-12 col-lg-8 left-side">
					<div class="post">
				
					<div class="row">
						<div class="col-md-2  post-date">
							<div class="day">
								<?php echo $day?>
							</div>
							<div class="month">
								<?php echo $month?>
							</div>
							<div class="year">
								<?php echo $year?>
							</div>
						</div>
						<div class="col-md-8 post-title">
							<a href="#"><h2><?php echo $title?></h2></a>
							<p>Written By <span><?php echo $author?></span></p>
						</div>
						<div class="col-md-2 profile-picture">
							<img src="img/<?php echo $author_image?>" alt="profile-picture" class="img-circle">
						</div>
					</div>
					<a href="#"><img src="img/<?php echo $image?>"></a>
					<p class="description"><?php echo $post_data?></p>
					
					<div class="bottom">
						<span class="Category-span"><i class="fa fa-folder-o" aria-hidden="true"></i>
							<a href="#"><?php echo $categories?></a>
						</span>
						<span class="comments-span"><i class="fa fa-comments-o" aria-hidden="true"></i>
							<a href="#">Comment</a>
						</span>
					</div>
					
				</div>
				<div class="related-posts">
				<h2>Related Posts</h2>
					<div class="row">
					<?php
						$r_query="SELECT * FROM posts  WHERE status='publish' and title LIKE '%$title%' LIMIT 3";
						$r_run=mysqli_query($connection,$r_query);
						while ($r_row=mysqli_fetch_array(	$r_run)) {
							$r_id=$r_row['id'];
							$r_title=$r_row['title'];
							$r_image=$r_row['image'];
							# code...
						
					?>
						<div class="col-md-4"><a href="post.php?id=<?php echo $r_id?>">
							<img src="img/<?php echo $r_image?> "alt="image">
							<h4><?php echo $r_title?></h4>
						</a></div>
						<?php }?>
					</div>
				</div>
				
				<div class="author">
					<div class="row">
					
					
					<div class="col-md-6" ><img src="img/<?php echo $author_image ?>" class="img-circle"></div>
					<div class="col-md-6"><p>
					<h2 >
						<?php echo $author?>
					</h2>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 


					</p></div>
					</div>

				</div>
				
				<div class="comments">
					<div class="row">
					<h3>Comments</h3>
						<div class="col-md-2">
							<img src="img/person.jpg" class="">
						</div>
						<div class="col-md-10">
							<h4>Mandeep singh</h4>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
							<p></p>
						</div>
					</div>
				</div>
					
				
				<div class="row comments-form">
						<div class="col-md-12 ">
						
							<form action="">
								<div class="form-group">
								<label for="full-name">
									Full Name:
								</label>	
								<input type="text" name="" id="full-name" class="form-control" placeholder="full-name">
								</div>
								<div class="form-group">
								<label for="full-name">
									Email
								</label>	
								<input type="text" name="" id="email" class="form-control" placeholder="Email">
								</div>
								<div class="form-group">
								<label for="full-name">
									Webiste
								</label>	
								<input type="text" name="" id="webiste" class="form-control" placeholder="website">
								</div>
								<div class="form-group">
								<label for="message">
									Comments
								</label>	
								<textarea id="message" cols="30" rows="10" placeholder="Write your messages Here" class="form-control">
									
								</textarea>
								<input type="button" name="" class="btn btn-primary submit" value="send comments" >
								</div>
							</form>
						</div>
					</div>
			</div>
				<div class="col-md-4 col-xs-12 col-lg-4">
											<?php include "include/sidebar.php" ; ?>
					
				</div>
			</div>
		</div>
	</section>
							<?php include "include/footer.php" ; ?>
</body>
</html>