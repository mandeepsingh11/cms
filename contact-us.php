<!DOCTYPE html>
<html>
<head>
							<?php include "include/head-data.php"  ?>
</head>
<body>
							<?php include "include/header.php"  ?>
	<div class="jumbotron jumbotron-div details">
		<div class="container">
			<h1 class="animated fadeInLeft">
				<span class="jumbotron-heading">Contact Us
			</h1>
			<p class="animated fadeInRight">
				Feel free to contact us We are always there for You
			</p>

		</div>
	</div>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-xs-12 col-lg-8 left-side">
					<div class="row">
						<div class="col-md-12">
							<div id="map"></div>
							<script type="text/javascript">
								function initMap()
								{
									var location ={lat:30.378179,lng:76.776697};
									var map =new google.maps.Map(document.getElementById('map'),{
										zoom:4,
										center:location
									});
								}
								var marker = new google.maps.Marker({
									position:location,
									map:map
								});
								
							</script>
							  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAcX1UIYFhMngkH_Njj8cLKp8NYd_Hp9Y&callback=initMap"
  type="text/javascript"></script>
						</div>
					</div>
					<div class="row Contact-form">
						<div class="col-md-12 ">
						<h2>Contact us</h2>
							<form action="">
								<div class="form-group">
								<label for="full-name">
									Full Name:
								</label>	
								<input type="text" name="" id="full-name" class="form-control" placeholder="full-name">
								</div>
								<div class="form-group">
								<label for="full-name">
									Email
								</label>	
								<input type="text" name="" id="email" class="form-control" placeholder="Email">
								</div>
								<div class="form-group">
								<label for="full-name">
									Webiste
								</label>	
								<input type="text" name="" id="webiste" class="form-control" placeholder="website">
								</div>
								<div class="form-group">
								<label for="message">
									Message
								</label>	
								<textarea id="message" cols="30" rows="10" placeholder="Write your messages Here" class="form-control">
									
								</textarea>
								<input type="button" name="" class="btn btn-primary submit" value="submit" >
								</div>
							</form>
						</div>
					</div>

				</div>
				<div class="col-md-4 col-xs-12 col-lg-4">
											<?php include "include/sidebar.php"  ?>

					</div>
					
				</div>
			</div>
		</div>
	</section>
							<?php include "include/footer.php"  ?>
</body>
</html>