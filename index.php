<!DOCTYPE html>
<html>
<head>
	<?php include 'include/head-data.php' ?>
</head>
<body>
	<?php include "include/header.php" ;
		$number_of_posts = 3;

		if (isset($_GET['page'])) {
			$page_id=$_GET['page'];
		}

		else
		{
			$page_id=1;
		}
		if(isset($_GET['cat']))
		{
			$cat_id	= $_GET['cat'];
			$cat_query="SELECT * FROM categories WHERE id =$cat_id";
			$cat_run=mysqli_query($connection,$cat_query);
			$cat_row=mysqli_fetch_array($cat_run);
			$cat_name	= $cat_row['category'];
		}


		if (isset($_POST['search'])) {
			$search=$_POST['search-title'];
			$fetch_post ="SELECT * FROM posts WHERE status ='publish'";
		
			$fetch_post.="and tags LIKE '%$search%'";
		
		$fetch_post_run =mysqli_query($connection,$fetch_post);
		$fetch_post_count=mysqli_num_rows($fetch_post_run);
		$total_pages=ceil($fetch_post_count/$number_of_posts);
		$posts_shown_from=($page_id-1)*$number_of_posts;
		}
		else
		{
			$fetch_post ="SELECT * FROM posts WHERE status ='publish'";
		if (isset($cat_name)) {
			$fetch_post.="and categories='$cat_name'";
		}
		$fetch_post_run =mysqli_query($connection,$fetch_post);
		$fetch_post_count=mysqli_num_rows($fetch_post_run);
		$total_pages=ceil($fetch_post_count/$number_of_posts);
		$posts_shown_from=($page_id-1)*$number_of_posts;
		}
		
	?>
	<div class="jumbotron jumbotron-div details">
		<div class="container">
			<h1 class="animated fadeInLeft">
				<span class="jumbotron-heading">Tech Site
			</h1>
			<p class="animated fadeInRight">
				We Provide all web development and seo services
			</p>

		</div>
	</div>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-xs-12 col-lg-8 left-side">
				<?php
					$slider_query = "SELECT * FROM posts WHERE status = 'publish ' ORDER BY id DESC";
					$sldier_run =mysqli_query($connection,$slider_query);
					if(mysqli_num_rows($sldier_run)> 0)
					{
						$count = mysqli_num_rows($sldier_run); 


				?>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
						  <ol class="carousel-indicators">
						    <?php 
						    
						    for($i=0;$i<$count;$i++){
						    	if($i == 0)
						    	{
						    		echo "<li data-target='#carousel-example-generic' data-slide-to='".$i."' class='active'></li>";
						    	}
						    	else
						    	{
						    		echo "<li data-target='#carousel-example-generic' data-slide-to='".$i."'></li>";	
						    	}
						    }

						    	?>
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						  <?php 
						  $check=0;
						  while($slider_row=mysqli_fetch_array($sldier_run))
						  {
						  	$slider_id = $slider_row['id'];
						  	$slider_title = $slider_row['title'];
						  	$slider_image = $slider_row['image'];
						  	$check=$check+1;
						  		if($check==1)
						  				{
						  					echo "<div class='item active'>";

						  				}
							  				else{
							  						echo "<div class='item'>";
						  				}		
						  
						  ?>
						    
						      <a href="post.php?post_id=<?php echo $slider_id;?>"><img src="img/<?php echo $slider_image?>" alt="slider-image"></a>
						      <div class="carousel-caption">
						        <h3><?php echo $slider_title?></h3>
						      </div>
						    </div>
						    
						   <?php
						   	}
						   ?>
						  </div>

  <!-- Controls -->
						  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
</div>
						<?php
						}

						if(isset($_POST['search']))
						{
							$search = $_POST['search-title'];
							$query = "SELECT * FROM posts WHERE status = 'publish' ";
						 
							$query.="and tags LIKE '%$searhc%'";
						
						$query .="ORDER BY id DESC LIMIT $posts_shown_from,$number_of_posts";
						}
						else
						{
							$query = "SELECT * FROM posts WHERE status = 'publish' ";
						if (isset($cat_name)) {
							$query.="and categories='$cat_name'";
						}
						$query .="ORDER BY id DESC LIMIT $posts_shown_from,$number_of_posts";
						}
						$run = mysqli_query($connection,$query);
						if(mysqli_num_rows($run)>0)
						{
							while($row=mysqli_fetch_array($run))
							{
								$id = $row['id'];
								$Date = getdate($row['Date']);
								$day = $Date['mday'];
								$month= $Date['month'];
								$year = $Date['year'];
								$title = $row['title'];
								$author = $row['author'];
								$author_image = $row['author_image'];
								$image = $row['image'];
								$categories = $row['categories'];
								$tags = $row['tags'];
								$post_data = $row['post_data'];
								$views = $row['views'];
								$status = $row['status'];
						?>
				<div class="post">
				
					<div class="row">
						<div class="col-md-2  post-date">
							<div class="day">
								<?php echo $day?>	
							</div>
							<div class="month">
								<?php echo $month?>
							</div>
							<div class="year">
								<?php echo $year?>
							</div>
						</div>
						<div class="col-md-8 post-title">
							<a href="post.php?post_id=<?php echo $id?>"><h2><?php echo $title?></h2></a>
							<p>Written By <span><?php echo $author?></span></p>
						</div>
						<div class="col-md-2 profile-picture">
							<img src="img/<?php echo $author_image?>" class="img-circle">
						</div>
					</div>
					<a href="post.php?post_id=<?php echo $id?>"><img src="img/<?php echo $image?>"></a>
					<p class="description"><?php echo $post_data?></p>
					<a href="post.php?post_id=<?php echo $id?>" class="btn btn-danger">Read More...</a>
					<div class="bottom">
						<span class="Category-span"><i class="fa fa-folder-o" aria-hidden="true"></i>
							<a href="#"><?php echo $categories?></a>
						</span>
						<span class="comments-span"><i class="fa fa-comments-o" aria-hidden="true"></i>
							<a href="#">Comment</a>
						</span>
					</div>
				</div>
				<?php
						}
						}
						else
						{
							echo "<center><h2>No Posts Availble Here</h2></center>";
						}

						?>	
				
				
				<nav aria-label="Page navigation" id="pagination">
					  <ul class="pagination">
					    <?php
					    	for ($i=1; $i <=$total_pages ; $i++) { 
									echo "<li class='".($page_id==$i ? 'active': '')."'><a href='index.php?page=".$i."'>$i</a></li>";	    		
					    	}
					     ?>
					  </ul>
				</nav>
			</div>

				<div class="col-md-4 col-xs-12 col-lg-4">
					
						<?php include "include/sidebar.php"  ?>

				</div>
					
				</div>
			</div>
		
	</section>
	<?php include "include/footer.php" ?>
</body>
</html>