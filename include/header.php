<?php

include 'include/db.php';
?>

<div class="navbar navbar-inverse navbar-static-top">
		<div class="container">
			<a href="index.php" class="navbar-brand">
				<span class="logo-text">
					Tech site
				</span>
			</a>
			<div class="navbar-header">
			<button class="navbar-toggle" data-toggle="collapse" 
			data-target=".navHeaderCollapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
				
			</button>
			</div>
			<div class="collapse navbar-collapse navHeaderCollapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="index.php" class="nav-item"><i class="fa fa-home" aria-hidden="true"></i>
Home</a></li>
					
					<li><a href="contact-us.php"><i class="fa fa-phone contact-space" aria-hidden="true"></i>Contact Us</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							Categories
						</a>
						<ul class="dropdown-menu">
							<?php
									$query = "SELECT * FROM categories ORDER BY id DESC";
									$run = mysqli_query($connection,$query);
									if(mysqli_num_rows($run)>0)
									{
										while($row= mysqli_fetch_array($run))
										{
											$category = ucfirst($row['category']);
											$id = $row['id'];
											echo"<li><a href='index.php?cat=".$id."'>$category</a></li>";
										}
									}
									else
									{
										echo "<a href='#'><li>No categories Yet</li></>";
									}
								?>
							
							
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
