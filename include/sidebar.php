
<div class="row search-bar">
							<div class="col-md-12">
							<form method="post" action="index.php">
								
							
								    <div class="input-group">
								      <input type="text" class="form-control" placeholder="Search for...">
								      <span class="input-group-btn">
								        <input class="btn btn-default" type="submit" value="Go">Go!
								      </span>
								    </div>
								 </form><!-- /input-group -->
							</div><!-- /.col-lg-6 -->
						</div>
						<div class="row">
							<div class="col-md-12 popular">
								<h4 class="popular-text">Popular Posts</h4>
								<?php
									$p_query="SELECT * FROM posts WHERE status = 'publish'
									ORDER BY views DESC LIMIT 5";
									$p_run=mysqli_query($connection,$p_query);
									if (mysqli_num_rows($p_run)>0) {
									
									while ($p_row=mysqli_fetch_array($p_run)) {
										$p_id=$p_row['id'];
										$p_image=$p_row['image'];
										$p_title=$p_row['title'];
										$p_date=getdate($p_row['Date']);
										 
								$p_day = $p_date['mday'];
								$p_month= $p_date['month'];
								$p_year = $p_date['year'];
									
								?>
								<hr>

								<div class="row">
									<div class="col-xs-4">
										<a href="post.php?post_id=<?php echo $p_id ?>"><img src="img/<?php echo $p_image ?>"></a>
									</div>
									<div class="col-xs-8">
										<h4>
											<a href="post.php?post_id=<?php echo $p_id; ?>"><?php echo $p_title ?></a> 
										</h4>
										<p>
											<i class="fa fa-clock-o" aria-hidden="true">
												
											</i>
											<?php echo" $p_day
												 $p_month
												 $p_year"?>
										</p>
									</div>
								</div>
								<?php }
							}
								else 
								{
									echo "<h3>No posts availabe</h3>";
									} 
								?>
								
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 popular">
								<h4 class="popular-text">Recent Posts</h4>
								<?php
									$p_query="SELECT * FROM posts WHERE status = 'publish'
									ORDER BY id DESC LIMIT 5";
									$p_run=mysqli_query($connection,$p_query);
									if (mysqli_num_rows($p_run)>0) {
									
									while ($p_row=mysqli_fetch_array($p_run)) {
										$p_id=$p_row['id'];
										$p_image=$p_row['image'];
										$p_title=$p_row['title'];
										$p_date=getdate($p_row['Date']);
										 
								$p_day = $p_date['mday'];
								$p_month= $p_date['month'];
								$p_year = $p_date['year'];
									
								?>
								<hr>

								<div class="row">
									<div class="col-xs-4">
										<a href="post.php?id=<?php echo $p_image ?>"><img src="img/<?php echo $p_image ?>"></a>
									</div>
									<div class="col-xs-8">
										<h4>
											<a href=""><?php echo $p_title ?></a> 
										</h4>
										<p>
											<i class="fa fa-clock-o" aria-hidden="true">
												
											</i>
											<?php echo" $p_day
												 $p_month
												 $p_year"?>
										</p>
									</div>
								</div>
								<?php }
							}
								else 
								{
									echo "<h3>No posts availabe</h3>";
									} 
								?>
								
							</div>
						</div>
						<div class="row categories">
						<h4 class="popular-text">Categories</h4>
							<div class="col-xs-6">
								<ul>
									<?php
										$c_query="SELECT * FROM categories ORDER BY id DESC";
										$c_run=mysqli_query($connection,$c_query);
										if(mysqli_num_rows($c_run)>0)
										{
											$count =2;
												while ($c_row=mysqli_fetch_array($c_run)) {
													$c_id=$c_row['id'];
													$c_category=$c_row['category'];
													$count = $count+1;
													if(($count%2)==1)
													{
														echo "<li><a href='index.php?cat=".$c_id."'>$c_category</a></li>";
													}
												}
										}
										else
										{
											echo "<p>no categories </p>";
										}
									?>
								</ul>
							</div>
							<div class="col-xs-6">
								<ul><?php
										$c_query="SELECT * FROM categories";
										$c_run=mysqli_query($connection,$c_query);
										if(mysqli_num_rows($c_run)>0)
										{
											$count =2;
												while ($c_row=mysqli_fetch_array($c_run)) {
													$c_id=$c_row['id'];
													$c_category=$c_row['category'];
													$count = $count+1;
													if(($count%2)==0)
													{
														echo "<li><a href='index.php?cat=".$c_id.">$c_category</a></li>";
													}
												}
										}
										else
										{
											echo "<p>no categories </p>";
										}
									?>
								</ul>
							</div>

						</div>
						<div class="row categories">
						<h4 class="popular-text">Socials Links</h4>
							<div class="col-xs-4"><a href=""><i class="fa fa-facebook-official fa-3x" aria-hidden="true"></i></a></div>
							<div class="col-xs-4"><a href=""><i class="fa fa-instagram fa-3x" aria-hidden="true"></i></a></div>
							<div class="col-xs-4"><a href=""><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></a></div>

						</div>
						<div class="row more-icons">
						
							<div class="col-xs-4 "><a href=""><i class="fa fa-snapchat-square fa-3x snapchat-icon " aria-hidden="true"></i></a></div>
							<div class="col-xs-4 "><a href=""><i class="fa fa-snapchat-square fa-3x snapchat-icon " aria-hidden="true"></i></a></div>
							<div class="col-xs-4"><a href=""><i class="fa fa-linkedin-square fa-3x" aria-hidden="true"></i></a></div>
						</div>